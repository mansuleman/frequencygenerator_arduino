#include <Arduino.h>

/* Uses SCPI parser library */
#include <scpiparser.h>


struct scpi_parser_context ctx;
float frequency;
char line_buffer[256];
unsigned char read_length;


scpi_error_t identify(struct scpi_parser_context* context, struct scpi_token* command);
scpi_error_t set_frequency(struct scpi_parser_context* context, struct scpi_token* command);

void setup()
{
  struct scpi_command* source;
  struct scpi_command* measure;
  pinMode(11, OUTPUT);

  /* First, initialise the parser. */
  scpi_init(&ctx);

  /*
   * After initialising the parser, we set up the command tree.  Ours is
   *
   *  *IDN?         -> identify
   *  :SOURCE
   *    :FREQuency  -> set_frequency
   *    :FREQuency? -> get_frequency
   */
   
  scpi_register_command(ctx.command_tree, SCPI_CL_SAMELEVEL, "*IDN?", 5, "*IDN?", 5, identify);
  scpi_register_command(ctx.command_tree, SCPI_CL_SAMELEVEL, "*RESET", 6, "*RST", 4, reset);
  
  source = scpi_register_command(ctx.command_tree, SCPI_CL_CHILD, "SOURCE", 6, "SOUR", 4, NULL);
  scpi_register_command(source, SCPI_CL_CHILD, "FREQUENCY", 9, "FREQ", 4, set_frequency);

  Serial.begin(9600);
}


void loop()
{
  while(1)
  {
    read_length = Serial.readBytesUntil('\n', line_buffer, 256);
    /* Read in a line and execute it. */
    if(read_length > 0)
    {
      scpi_execute_command(&ctx, line_buffer, read_length);
    }
  }
}

/*
 * *IDN?
 */
scpi_error_t identify(struct scpi_parser_context* context, struct scpi_token* command)
{
  scpi_free_tokens(command);

  Serial.println("Arduino Nano Module");
  return SCPI_SUCCESS;
}


/*
 * *RST
 */
scpi_error_t reset(struct scpi_parser_context* context, struct scpi_token* command)
{
  scpi_free_tokens(command);

  asm volatile ("  jmp 0");
  Serial.println("Program reset");
  return SCPI_SUCCESS;
}

/*
 * :FREQUENCY
 */
scpi_error_t set_frequency(struct scpi_parser_context* context, struct scpi_token* command)
{
  
  struct scpi_token* args;
  struct scpi_numeric output_numeric;
  unsigned char output_value;
  
  args = command;

  while(args != NULL && args->type == 0)
  {
    args = args->next;
  }

  output_numeric = scpi_parse_numeric(args->value, args->length, 1e3, 0, 25e6);

  if(output_numeric.length == 0 ||
    (output_numeric.length == 2 && output_numeric.unit[0] == 'H' && output_numeric.unit[1] == 'z'))
  {
      frequency = (unsigned long)constrain(output_numeric.value, 0, 25e6);
      Serial.print("frequency = ");
      Serial.println(frequency);   

  // Delay in hrtz of 1 ms = 1/0.001 = 1000 Hz… 

  while (read_length > 0) {
     digitalWrite(11, HIGH); 
     delay(1/frequency * 1000); 
     digitalWrite(11, LOW);
     delay(1/frequency * 1000);
     if (Serial.read() > 0) break;
  }   
    
  }
  else
  {
    scpi_error error;
    error.id = -200;
    error.description = "Command error;Invalid unit";
    error.length = 26;
    
    scpi_queue_error(&ctx, error);
    scpi_free_tokens(command);
    return SCPI_SUCCESS;
  }

  
  scpi_free_tokens(command);  
  
  return SCPI_SUCCESS;
}


