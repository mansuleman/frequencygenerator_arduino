# README #

This is the Frequency Generator module for an Independent study. This part of the study runs on Arduino and allows to send SCPI commands to Frequency Generator.

### Which SCPI commands are implemented ###

* *IDN? - name of the module
* *RST - reset the module
* :SOURCE:FREQ XXHz - set the frequency

### How do I test the frequency ###

* Run this code on your Frequency generator Arduino
* Connect your FG arduino with oscilloscope
* As an alternative to real oscilloscope you can use another Arduino (instruction here: https://github.com/konsumer/arduinoscope)
